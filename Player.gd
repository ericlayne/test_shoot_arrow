extends RigidBody2D

const FIRING_TIMER = .25
var firing_countdown = 0.0
var impulse_shoot_speed = 10
var velocity_shoot_speed = 50

var bullet = preload("res://bullet.tscn")
var key_up = false
var key_down = false
var key_left = false
var key_right = false
var mouse_down = false

var mouse_pos
var mouse_vector
var sprite_node


func _ready():
	set_process_input(true)
	set_process(true)
	sprite_node = get_node("Sprite3")


func _process(delta):
	firing_countdown += delta
	
	mouse_pos = get_global_mouse_pos()
	mouse_vector = mouse_pos - self.get_global_pos()
	sprite_node.set_rot(get_angle_to(mouse_pos))
	
	if key_up: apply_impulse(Vector2(),Vector2(0,-20))
	if key_down: apply_impulse(Vector2(),Vector2(0,20))
	if key_left: apply_impulse(Vector2(),Vector2(-20,0))
	if key_right: apply_impulse(Vector2(),Vector2(20,0))
	if mouse_down and firing_countdown >= FIRING_TIMER: _shoot_bullet(delta)
	
	
func _input(event):
	if event.is_action_pressed("ui_up"): key_up = true
	if event.is_action_released("ui_up"): key_up = false
	
	if event.is_action_pressed("ui_down"): key_down = true
	if event.is_action_released("ui_down"): key_down = false
	
	if event.is_action_pressed("ui_left"): key_left = true
	if event.is_action_released("ui_left"): key_left = false
	
	if event.is_action_pressed("ui_right"): key_right = true
	if event.is_action_released("ui_right"): key_right = false
	
	if event.is_action_pressed("mouse_down"): mouse_down = true
	if event.is_action_released("mouse_down"): mouse_down = false
	
func _shoot_bullet(delta):
	var new_bullet = bullet.instance()
	var bullet_rotation = get_angle_to(mouse_pos) + self.get_rot()
	new_bullet.set_rot(bullet_rotation)
	new_bullet.set_global_pos(self.get_global_pos())
	get_parent().add_child(new_bullet)
	
	var rigidbody_vector = (mouse_pos - self.get_pos()).normalized()
	var mouse_distance = self.get_pos().distance_to(mouse_pos)
	new_bullet.set_linear_velocity(rigidbody_vector * velocity_shoot_speed * mouse_distance * delta)
	#new_bullet.apply_impulse(Vector2(), mouse_vector * impulse_shoot_speed)

	firing_countdown = 0